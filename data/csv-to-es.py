#!/usr/bin/env python

import csv
from datetime import datetime
import requests
import sys
from time import sleep

ELASTIC_SEARCH_BASE = sys.argv[1]
CSV_INPUT = sys.argv[2]
ES_INDEX_URL = ELASTIC_SEARCH_BASE + '/locations-training'

while True:
    try:
        requests.get(ELASTIC_SEARCH_BASE)
        print('Connected to %s' % ELASTIC_SEARCH_BASE)
        break
    except requests.exceptions.ConnectionError as e:
        print('%s retrying in 10s' % e)
    sleep(10)

esrecords = []
with open(CSV_INPUT) as f:
    reader = csv.DictReader(f)
    for d in reader:
        dt = datetime.strptime(
            d['Date-dd/mm'] + '-' + d['Date - year'], '%d-%b-%Y')
        esr = dict(
            EventType='training',
            City=d['City'],
            Country=d['Country'],
            location=dict(
                lat=float(d['Latitude']),
                lon=float(d['Longitude']),
            ),
            Date=dt.strftime('%Y-%m-%d'),
        )
        esrecords.append(esr)

# Create ES index if it doesn't exist
r = requests.get(ES_INDEX_URL)
if r:
    print('Index already exists')
else:
    r = requests.put(ES_INDEX_URL, json={})
    assert r, r.text

# Create ES index mapping
r = requests.put(ES_INDEX_URL + '/_mapping' + '/training', json=dict(
    properties=dict(
        EventType=dict(type="keyword"),
        City=dict(type="keyword"),
        Country=dict(type="keyword"),
        location=dict(type="geo_point"),
        Date=dict(type="date"),
    )
))
assert r, r.text

for esr in esrecords:
    id = esr['City'] + esr['Country'] + esr['Date']
    # print id, esr
    print(id)
    r = requests.post(ES_INDEX_URL + '/training/' + id, json=esr)
    assert r, r.text

print('Completed!')
